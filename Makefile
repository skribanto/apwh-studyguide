BIBCC=bibtex studyguide
PREFLAGS=-draftmode
FLAGS=-file-line-error
DEPS=period1.tex period2.tex period3.tex period4.tex period5.tex period6.tex importantdates.tex
PCC=pdflatex $(PREFLAGS) $(FLAGS) studyguide.tex
CC=pdflatex $(FLAGS) studyguide.tex

studyguide: studyguide.tex $(DEPS)
	$(PCC) && $(BIBCC) && $(PCC) ; $(CC)

clean:
	rm *.aux *.log *.bbl *.blg *.out *.toc 2> /dev/null
